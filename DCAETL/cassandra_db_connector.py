from datetime import date
from my_cassandrapanda import MyCassandraDB

def date_time():
    cur_date = date.today().isoformat()
    return cur_date

def main():

    cassPanda = MyCassandraDB('admin', 'h1t4chi1', 'localhost', 'casscdb')
    cassPanda.connect_with_Auth()

    file1 = 'cass_cap_data.txt'
    cql = "INSERT INTO pool_capacity (agent_pool_key, data_date, cur_date, free_capacity, " \
          "managed_capacity, pool_id, storage_agent, total_capacity, used_capacity, " \
          "used_percent) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    cassPanda.cql_insert_from_file(file1, cql)

    # output = cassPanda.cql_execute('SELECT * FROM pool_capacity')
    # for line in output:
    #     print(line)


main()
