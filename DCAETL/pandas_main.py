'''
Scope
Automation of billing report and possible display of content/graphs for local viewing.
Work scope is at a Storage system Pool level or higher, ie storage unit, site, company.

etl methods:
using "http gets" and api extraction then formatting  output to desired view.

Storing:
store data in a persistent format easily accessible and secure that it wont be deleted or modified.
mysql - MariaDB, mysql

viewing: - in order of importance and implmentation
1. import data from MariaDB to pre-configured billing reports that can be run daily if needed.
2. Display information via tableau on desktops able to connect to the database or extracts from it.
3. web viewing of capacity and trending at pool levels.
    - basic gui with landing page, couple of options and information display pages.

languages:
OOP - Python 3.5/3.6
Database - MariaDB/mySql
web - django/python3.5
'''


import requests
import pandas as pd
# from pandas import ExcelWriter
from openpyxl import load_workbook
import pickle
from datetime import date

def agent_csv_read():
    file_name = 'agent.csv'
    df = pd.read_csv(file_name)
    return df

def pool_csv_read():
    file_name = 'raidpdplc_MDCVSP11.csv'
    df = pd.read_csv(file_name)
    return df

def get_url(url, host, instance):
    url_sub = url + '?hostName=' + host + '&agentInstanceName=' + instance
    url_main_1 = 'http://ulpiaa001.auiag.corp:22015/Analytics/RAIDAgent/v1/objects/' + url_sub
    return url_main_1

def get_requests_data(url):
    # df_temp = pd.DataFrame
    auth1 = ('apiUser', 'apiView')
    data = requests.get(url, auth=auth1)
    # df_temp = data.content
    # df_temp.to_pickle('api.pickle')
    file = open('temp.txt', 'wb')
    file.write(data.content)
    file.close()

def df_to_wind_notExcel(dataframe):
    cur_date = date_time()
    sheet_name = 'Data_' + cur_date
    file_xls = 'IAG_Disk_Capacity_Template.xlsx'

    '''below section is to write to a new file each time'''
    # writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
    # dataframe.to_excel(writer, sheet_name='Sheet5')
    # workbook = writer.book
    # worksheet = writer.sheets['Sheet5']
    # format1 = workbook.add_format({'num_format': '##########.##'})
    # format2 = workbook.add_format({'num_format': '0%'})
    # worksheet.set_column('E:E', 25, format1)
    # writer.save()

    '''below is to write to an existing file without overwriting other tabs/sheets'''
    with pd.ExcelWriter(file_xls, engine='openpyxl') as writer:
        writer.book = load_workbook(file_xls)
        dataframe.to_excel(writer, sheet_name=sheet_name)
    # add in format if needed, here.
    writer.save()


def date_time():
    cur_date = date.today().isoformat()
    return cur_date


def main():

    df_agent = agent_csv_read()[['instanceName', 'hostName']]
    df_pool_list_main = pd.DataFrame([])

    for index, row, in df_agent.iterrows():
        print('*******************NEW*****************')
        host = row['hostName']
        instance = row['instanceName']
        print(host, instance)

        url_sub_ldevs = '/RAID_PD_VVC?hostName='
        url_sub_pools = '/RAID_PD_PLC'
        pools_url = get_url(url_sub_pools, host, instance)
        print(pools_url)

        # get_requests_data(pools_url)
        # df_pool_requests_data = pd.read_pickle('api.pickle')

        # file_name_temp = 'temp'+str(num)+'.txt'
        # df_pool_requests_data = pd.read_csv(file_name_temp)
        # num += 1
        # print(num)

        df_pool_requests_data = pd.read_csv('temp.txt')


        df_pool_requests_data['STORAGE_AGENT'] = instance
        df_pool_requests_data['AGENT_POOL_KEY'] = df_pool_requests_data['STORAGE_AGENT'] + '-' + df_pool_requests_data['POOL_ID']
        df_pool_list = df_pool_requests_data[1:][['AGENT_POOL_KEY', 'STORAGE_AGENT', 'POOL_ID', 'TOTAL_ACTUAL_CAPACITY', 'USED_CAPACITY', 'TOTAL_MANAGED_CAPACITY',
             'FREE_CAPACITY']]

        if not df_pool_list_main.empty:
            df_pool_list_main = df_pool_list_main.append(df_pool_list, ignore_index=True)
        else:
            df_pool_list_main = df_pool_list

        print('***************************************')

    df_to_wind_notExcel(df_pool_list_main)
    print(df_pool_list_main)

    # print(df_pool_list[1:])
    # print(df_pool[1:][1:])



main()