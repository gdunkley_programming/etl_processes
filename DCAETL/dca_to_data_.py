import requests
import pandas as pd
from pandas import ExcelWriter
from openpyxl import load_workbook
from datetime import date
import ijson
import os.path


def get_url(url_num, **kwargs):
    url = kwargs.get('url', '')
    host = kwargs.get('host', '')
    inst = kwargs.get('instance', '')
    dest_path = kwargs.get('dest', '')
    cur_date = date.today()
    formateddate = cur_date.strftime('%Y%m%d')

    url_sub = url + '?hostName=' + str(host) + '&agentInstanceName=' + str(inst)
    url_main_1 = 'http://ulpiaa001.auiag.corp:22015/Analytics/RAIDAgent/v1/objects/' + url_sub
    url_main_2 = 'http://ulpiaa001.auiag.corp:22015/Analytics/RAIDAgent/v1/objects/AgentForRAID'

    url_dca_main_3 = "curl -k -1 " \
                 "-H \'Authorization: Basic Z2R1bmtsZXk6SCF0NGNoaTE=\' " \
                 "\'https://ulpdca001.auiag.corp:8443/dbapi.do?action=query&dataset=defaultDs&processSync=true\' " \
                 "-H \'Content-Type: application/json\' " \
                 "-d \'{\"query\":\"*raidStorage[=name rx .*]/raidPG[=pgType rx .*]&[=displayDriveType rx .*]\"," \
                 "\"startTime\":\" " + formateddate + "_000000\"," \
                                                      "\"endTime\":\" " + formateddate + "_000001\"}\' " \
                                                                                         "> " + dest_path + "raidPG.json"

    if url_num == 1:
        url_main = url_main_1
    elif url_num == 2:
        url_main = url_main_2
    elif url_num == 3:
        url_main = url_dca_main_3
    return url_main


def get_requests_tofile(url):
    # df_temp = pd.DataFrame
    auth1 = ('apiUser', 'apiView')
    data = requests.get(url, auth=auth1)
    # df_temp = data.content
    # df_temp.to_pickle('api.pickle')
    file = open('temp.txt', 'wb')
    file.write(data.content)
    file.close()


def get_requests_todf(url):
    auth1 = ('apiUser', 'apiView')
    data = pd.read_json(requests.get(url, auth=auth1).content)
    return data


def df_to_wind_notExcel(dataframe):
    cur_date = date_time()
    sheet_name = 'Data_' + cur_date
    file_xls = 'IAG_Capacity_Planning.xlsx'

    '''below section is to write to a new file each time'''
    # writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
    # dataframe.to_excel(writer, sheet_name='Sheet5')
    # workbook = writer.book
    # worksheet = writer.sheets['Sheet5']
    # # format1 = workbook.add_format({'num_format': '##########.##'})
    # # format2 = workbook.add_format({'num_format': '0%'})
    # # worksheet.set_column('E:E', 25, format1)

    '''below is to write to an existing file without overwriting other tabs/sheets'''
    with pd.ExcelWriter(file_xls, engine='openpyxl') as writer:
        writer.book = load_workbook(file_xls)
        dataframe.to_excel(writer, sheet_name=sheet_name)
    # add in format if needed, here.
    writer.save()


def df_to_csv(dataframe, filename):
    # file = filename
    dataframe.to_csv(filename, index=False)


def csv_to_df(filename):
    dataframe = pd.read_csv(filename)
    return dataframe


def date_time():
    cur_date = date.today().isoformat()
    return cur_date


def json_file_extraction(filename):
    file = filename
    with open(file, 'r') as f:
        # modification of the objects within json is needed...
        objects = ijson.items(f, 'items.item')
        columns = list(objects)
    df_1 = pd.DataFrame(columns)

    # modification of columns for specific values is needed...
    df_2 = df_1[['instanceName', 'hostName']]
    return df_2


def main():
    url2 = get_url(3)
    get_requests_tofile(url2)

    # os.path.
    df_agent = csv_to_df('agent_list.txt')

    df_raid_list_main = pd.DataFrame([])

    for index, row, in df_agent.iterrows():
        print('*******************NEW*****************')
        host = row['hostName']
        instance = row['instanceName']
        print(host, instance)

        url_sub_rgrp = '/RAID_PD_LDC'
        url_sub_ldevs = '/RAID_PD_VVC'

        raid_url = get_url(1, url=url_sub_rgrp, host=host, instance=instance)
        print(raid_url)
        get_requests_tofile(raid_url)

        df_raid_requests_data = pd.read_csv('temp.txt')

        '''
        Cassandra Formating for cql database import if needed
        '''

        df_raid_requests_data['STORAGE_AGENT'] = instance
        df_raid_requests_data['AGENT_RAID_GROUP_NUMBER'] = "'" + df_raid_requests_data['STORAGE_AGENT'] + '-' + \
                                                           df_raid_requests_data['RAID_GROUP_NUMBER'] + "'"

        df_raid_requests_data['DATETIME'] = "'" + df_raid_requests_data['DATETIME'] + "'"
        df_raid_requests_data['RAID_GROUP_NUMBER'] = "'" + df_raid_requests_data['RAID_GROUP_NUMBER'] + "'"
        df_raid_requests_data['LDEV_NAME'] = "'" + df_raid_requests_data['LDEV_NAME'] + "'"

        df_raid_list = df_raid_requests_data[1:][['AGENT_RAID_GROUP_NUMBER', 'DATETIME', 'LDEV_NUMBER',
                                                  'RAID_GROUP_NUMBER', 'RAID_LEVEL', 'RAID_TYPE',
                                                  'LDEV_NAME', 'VOLUME_TYPE', 'POOL_ID']]

        if not df_raid_list_main.empty:
            df_raid_list_main = df_raid_list_main.append(df_raid_list, ignore_index=True)
        else:
            df_raid_list_main = df_raid_list

        print('***************************************')

    # df_to_wind_notExcel(df_raid_list_main)

    file_output_name1 = 'raid_data.txt'

    df_to_csv(df_raid_list_main, file_output_name1)
    print(df_raid_list_main.head())


main()