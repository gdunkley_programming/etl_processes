import requests
import pandas as pd
from pandas import ExcelWriter
from openpyxl import load_workbook
from datetime import date
import ijson


def get_url(url_num, **kwargs):
    url = kwargs.get('url', '')
    host = kwargs.get('host', '')
    inst = kwargs.get('instance', '')
    url_sub = url + '?hostName=' + str(host) + '&agentInstanceName=' + str(inst)
    url_main_1 = 'http://ulpiaa001.auiag.corp:22015/Analytics/RAIDAgent/v1/objects/' + url_sub
    url_main_2 = 'http://ulpiaa001.auiag.corp:22015/Analytics/RAIDAgent/v1/objects/AgentForRAID'
    if url_num == 1:
        url_main = url_main_1
    elif url_num == 2:
        url_main = url_main_2
    return url_main


def get_requests_tofile(url):
    # df_temp = pd.DataFrame
    auth1 = ('apiUser', 'apiView')
    data = requests.get(url, auth=auth1)
    # df_temp = data.content
    # df_temp.to_pickle('api.pickle')
    file = open('temp.txt', 'wb')
    file.write(data.content)
    file.close()


def get_requests_todf(url):
    auth1 = ('apiUser', 'apiView')
    data = pd.read_json(requests.get(url, auth=auth1).content)
    return data


def df_to_wind_notExcel(dataframe):
    cur_date = date_time()
    sheet_name = 'Data_' + cur_date
    file_xls = 'IAG_Capacity_Planning.xlsx'

    '''below section is to write to a new file each time'''
    # writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
    # dataframe.to_excel(writer, sheet_name='Sheet5')
    # workbook = writer.book
    # worksheet = writer.sheets['Sheet5']
    # # format1 = workbook.add_format({'num_format': '##########.##'})
    # # format2 = workbook.add_format({'num_format': '0%'})
    # # worksheet.set_column('E:E', 25, format1)

    '''below is to write to an existing file without overwriting other tabs/sheets'''
    with pd.ExcelWriter(file_xls, engine='openpyxl') as writer:
        writer.book = load_workbook(file_xls)
        dataframe.to_excel(writer, sheet_name=sheet_name)
    # add in format if needed, here.
    writer.save()


def df_to_csv(dataframe, filename):
    # file = filename
    dataframe.to_csv(filename, index=False)


def csv_to_df(filename):
    df = pd.read_csv(filename)
    return df



def date_time():
    cur_date = date.today().isoformat()
    return cur_date


def json_file_extraction(filename):
    file = filename
    with open(file, 'r') as f:
        # modification of the objects within json is needed...
        objects = ijson.items(f, 'items.item')
        columns = list(objects)
    df_1 = pd.DataFrame(columns)

    # modification of columns for specific values is needed...
    df_2 = df_1[['instanceName', 'hostName']]
    return df_2


def main():
    url2 = get_url(2)
    get_requests_tofile(url2)

    # df_agent = json_file_extraction('temp.txt')
    # df_to_csv(df_agent, 'agent_list.txt')
    df_agent = csv_to_df('agent_list.txt')



    df_pool_list_main = pd.DataFrame([])
    for index, row, in df_agent.iterrows():
        print('*******************NEW*****************')
        host = row['hostName']
        instance = row['instanceName']
        print(host, instance)

        url_sub_ldevs = '/RAID_PD_VVC?hostName='
        url_sub_pools = '/RAID_PD_PLC'
        pools_url = get_url(1, url=url_sub_pools, host=host, instance=instance)
        print(pools_url)
        get_requests_tofile(pools_url)

        df_pool_requests_data = pd.read_csv('temp.txt')

        '''
        #Cassandra Formating for cql database import
        '''
        # df_pool_requests_data['STORAGE_AGENT'] = instance
        # df_pool_requests_data['AGENT_POOL_KEY'] = "'" + df_pool_requests_data['STORAGE_AGENT'] + '-' + \
        #                                           df_pool_requests_data['POOL_ID'] + "'"
        # df_pool_requests_data['RECORD_TIME'] = "'" + df_pool_requests_data['RECORD_TIME'] + "'"
        # df_pool_requests_data['DATETIME'] = "'" + df_pool_requests_data['DATETIME'] + "'"
        # df_pool_requests_data['STORAGE_AGENT'] = "'" + df_pool_requests_data['STORAGE_AGENT'] + "'"
        #
        # df_pool_list = df_pool_requests_data[1:][['AGENT_POOL_KEY', 'RECORD_TIME', 'DATETIME', 'FREE_CAPACITY',
        #                                           'TOTAL_MANAGED_CAPACITY', 'POOL_ID', 'STORAGE_AGENT',
        #                                           'TOTAL_ACTUAL_CAPACITY',
        #                                           'USED_CAPACITY', 'USAGE_RATE']]



        '''
        #Capacity Planning Formatting
        '''
        df_pool_requests_data['STORAGE_AGENT'] = instance
        df_pool_requests_data['AGENT_POOL_KEY'] = df_pool_requests_data['STORAGE_AGENT'] + '-' + df_pool_requests_data['POOL_ID']
        df_pool_list = df_pool_requests_data[1:][['AGENT_POOL_KEY', 'STORAGE_AGENT',
                                                  'POOL_ID', 'TOTAL_ACTUAL_CAPACITY',
                                                  'USED_CAPACITY', 'TOTAL_MANAGED_CAPACITY',
                                                  'FREE_CAPACITY']]


        '''
        #History Capacity Formatting
        '''
        # df_pool_requests_data['STORAGE_AGENT'] = instance
        # df_pool_requests_data['AGENT_POOL_KEY'] = "'" + df_pool_requests_data['STORAGE_AGENT'] + '-' + \
        #                                           df_pool_requests_data['POOL_ID'] + "'"
        # df_pool_requests_data['RECORD_TIME'] = "'" + df_pool_requests_data['RECORD_TIME'] + "'"
        # df_pool_requests_data['DATETIME'] = "'" + df_pool_requests_data['DATETIME'] + "'"
        # df_pool_requests_data['STORAGE_AGENT'] = "'" + df_pool_requests_data['STORAGE_AGENT'] + "'"
        #
        # df_pool_list = df_pool_requests_data[1:][['AGENT_POOL_KEY', 'STORAGE_AGENT', 'POOL_ID', 'TOTAL_ACTUAL_CAPACITY',
        #                                           'USED_CAPACITY', 'TOTAL_MANAGED_CAPACITY', 'FREE_CAPACITY',
        #                                           'RECORD_TIME']]

        if not df_pool_list_main.empty:
            df_pool_list_main = df_pool_list_main.append(df_pool_list, ignore_index=True)
        else:
            df_pool_list_main = df_pool_list

        print('***************************************')

    # df_to_wind_notExcel(df_pool_list_main)

    file_output_name1 = 'cass_cap_data.csv'
    file_output_name2 = 'cap_plan_data.txt'
    file_output_name3 = 'hist_cap_data.txt'

    df_to_csv(df_pool_list_main, file_output_name2)
    print(df_pool_list_main.head())

main()
