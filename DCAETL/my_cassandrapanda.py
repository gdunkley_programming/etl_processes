'''
Cassandra Class for all cassandra DB functions
'''

from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
import pandas as pd

class MyCassandraDB:


    def __init__(self, user, password, cluster_node, keyspace):
        self.usr = user
        self.pwd = password
        self.clstr = cluster_node
        self.keyspace = keyspace
        self.session = object

    def connect(self):
        cluster = Cluster([self.clstr])
        self.session = cluster.connect(keyspace=self.keyspace)

    def connect_with_Auth(self):
        authorisation = PlainTextAuthProvider(username=self.usr, password=self.pwd)
        cluster = Cluster([self.clstr], auth_provider=authorisation)
        self.session = cluster.connect(keyspace=self.keyspace)

    def cql_execute(self, cqlquery):
        return self.session.execute(cqlquery)

    def cql_insert_from_file(self, file, cql):
        df_cass = pd.read_csv(file)
        for index, df_cass_row in df_cass.iterrows():
            col_0 = df_cass_row[0]
            col_1 = pd.to_datetime(df_cass_row[1])
            col_2 = pd.to_datetime(df_cass_row[2])
            col_3 = pd.to_numeric(df_cass_row[3], downcast='float')
            col_4 = pd.to_numeric(df_cass_row[4], downcast='float')
            col_5 = pd.to_numeric(df_cass_row[5])
            col_6 = df_cass_row[6]
            col_7 = pd.to_numeric(df_cass_row[7], downcast='float')
            col_8 = pd.to_numeric(df_cass_row[8], downcast='float')
            col_9 = pd.to_numeric(df_cass_row[9], downcast='float')
            row_vals = (col_0, col_1, col_2, col_3, col_4, col_5, col_6, col_7, col_8, col_9)
            self.session.execute(self.session.prepare(cql), row_vals)








