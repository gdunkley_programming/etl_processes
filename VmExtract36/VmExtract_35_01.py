'''
1. Problem statement
====================

Unable to determine what Ldevs have changed in capacity and therefor unable to determine which cleanup process
has succeded or not and from which part of the company

2. Defining Diagram
===================

Input                               Processing                              Output
-----                               ----------                              ------
export of CMDB in CSV format        MySQL search for details by line        CSV values and corresponding SQL statment details.


3. Pseudocode
=============

Start

check file CSV exists
create new file for output, date,name different for each run.
Open file (CSV)

Loop for each line in CSV file

    Read Next line of CSV
        set parameters from CSV file for variables in sql command
        issue mySql Command with line parameters (will need access/ remote sqlcommand for database)
        Filter/ retrieve specific output details (capacity/ capacity used) in new variables
        Output/write line of required fields from CSV file and new retrieved sql details appending to new file.


close loop

output activity log

End

4. Test Runs
============

Normal output
-------------



Abnormal output
---------------



5. Error Codes
==============

'''
import my_py_util
import csv
import os
import logging

def main():
    import pymysql.cursors


    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    # create a file handler
    handler = logging.FileHandler('VmExtract36.log')
    handler.setLevel(logging.INFO)
    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(handler)

    logger.info('Hello world')

    #file_name_in = os.path.abspath("report-vmmdc.auiag.corp-201709191307-1")
    file_name_in = os.path.abspath("report-vmwmdc.auiag.corp-201709191307-1")
    file_name_out = os.path.abspath(file_name_in)

    # today_date = datetime.date.today()
    today_date = '2017-09-08'

    file1 = file_name_in + ".csv"
    file2 = file_name_out + "_" + today_date + ".csv"


    chk_file_status = my_py_util.chk_file(file1, file2)
    if chk_file_status == False:
        return

    with open(file1) as input_file, open(file2, 'w') as output_file:
        csv_input = csv.reader(input_file)
        writer = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        row1 = next(csv_input)
        writer.writerow(row1)

        for line in csv_input:
            #sub_serial = my_py_util.line_ref_subsys(line[-3]) # hashed as csv now has a dec serial Number rather then name.
            dec_sub_serial = line[-2]
            dec_ldev = my_py_util.line_ref_ldev(line[-1])

            # Setting values to NULL for mysql where csv file has no entrys... enables sql to continue.
            if dec_sub_serial is None or dec_ldev is None:
                dec_sub_serial = 'NULL'
                dec_ldev = 'NULL'

            connection = my_py_util.get_connection()
            mysql_query = (
                "SELECT `ldev_cap`, `ldev_usdcap` FROM `Ldevs` WHERE `sub_serial`=%s "
                "AND `ldev_id`=%s AND `data_date`=%s")

            try:
                with connection.cursor() as cursor:
                    cursor.execute(mysql_query, (dec_sub_serial, dec_ldev, today_date))
                    while True:
                        result = cursor.fetchone()
                        if not result:
                            break

                        line.append(result['ldev_cap'])
                        line.append(result["ldev_usdcap"])
                        writer.writerow(line)

            finally:
                connection.close()


        input_file.close()
        output_file.close()

if __name__ == '__main__':
    #import logging.config
    #logging.config.fileConfig('/path/to/logging.conf')
    main()
