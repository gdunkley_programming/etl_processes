def main():
    file1 = 'report-vmhpdr-1.csv'
    file2 = 'OutputFile_old.csv'
    day_date = '2017-09-05'

    etl(file1, file2, day_date)


def etl(file1, file2, day_date):
    with open(file1, 'r') as report_input, open(file2, 'w') as report_output:
        title_row1 = report_input.readline()
        report_output.write(title_row1)

        for report_input_line in report_input:
            print("***********")
            split_line = report_input_line.strip().split(",")
            subsys = split_line[-2]
            print(subsys, " - ", type(subsys), "-subsys")

            if subsys == '"WDC_VSP_01"':
                subSerialNum = 55841
                print(subSerialNum, " - ", type(subsys), "-subSerialNum")
            elif subsys == '"WDC_VSP_11"':
                subSerialNum = 51206
                print(subSerialNum, " - ", type(subsys), "-subSerialNum")
            elif subsys == '"WDC_VSP_03"':
                subSerialNum = 55953
                print(subSerialNum, " - ", type(subsys), "-subSerialNum")

            ldev = str(int(split_line[-1].replace(':', '').strip('"'), base=16))
            print(ldev, " - ", type(ldev), "-ldev")
            print(day_date, type(day_date), "-day_date")

            query_result = connector_mysql(subSerialNum, ldev, day_date)
            print(query_result, " - ", type(query_result))

            ldev_cap_result = query_result['ldev_cap']
            print(ldev_cap_result, " - ", type(ldev_cap_result))

            # ldev_usdcap_result = query_result['ldev_usdcap']
            # print(ldev_usdcap_result, " - ", type(ldev_usdcap_result))


def connector_mysql(sub_serial_num, ldev, today_date):
    import pymysql.cursors

    db_server = 'localhost'
    db_name = 'CBDB'
    db_pass = 'securePassword'
    db_user = 'graham'

    sql_query = (
        "SELECT ldev_cap, ldev_usdcap FROM Ldevs WHERE sub_serial=%(serial)s "
        "and ldev_id=%(lun)s and data_date=%(todayD)s")

    connection = pymysql.connect(host=db_server,
                                 user=db_user,
                                 password=db_pass,
                                 db=db_name,
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            cursor.execute(sql_query, {'serial': sub_serial_num, 'lun': ldev, 'todayD': today_date})
            result = cursor.fetchone()
            return result

    finally:
        connection.close()


main()
