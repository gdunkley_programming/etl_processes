import pandas as pd
from openpyxl import load_workbook


def df_to_wind_notExcel(dataframe, year):
    sheet = 'Data_' + year
    xlsx_file = 'CapData.xlsx'

    '''below is to write to an existing file without overwriting other tabs/sheets'''
    with pd.ExcelWriter(xlsx_file, engine='openpyxl') as writer:
        writer.book = load_workbook(xlsx_file)
        dataframe.to_excel(writer, sheet_name=sheet)
    # add in format if needed, here.
    writer.save()


def col_extract(year, months, yr):
    num = 1
    df_col_extract = pd.DataFrame()

    while num <= months:

        num_format = '{:02}'.format(num)
        print('Month: ' + str(num_format))

        file_name = year + '/IAG_Disk_Capacity_20-' + str(num_format) + '-' + str(yr) + '.xlsx'
        df = pd.read_excel(file_name, sheetname='Numbers')
        file_date = '20/' + str(num_format) + '/' + str(yr)

        # print(df.head())
        # print('************************************')

        if year == '2013':
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 7', 'Unnamed: 8', 'Unnamed: 9',
                                   'Unnamed: 10', 'Unnamed: 11']]
        elif year == '2014':
            # needs to be confirmed
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 7', 'Unnamed: 8', 'Unnamed: 9',
                                   'Unnamed: 10', 'Unnamed: 11']]
        elif year == '2015':
            # needs to be confirmed
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 2', 'Unnamed: 3', 'Unnamed: 7', 'Unnamed: 8', 'Unnamed: 9',
                                   'Unnamed: 10', 'Unnamed: 11']]
        elif year == '2016':
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 14', 'Unnamed: 15', 'Unnamed: 16', 'Unnamed: 17',
                                   'Unnamed: 18', 'Unnamed: 19', 'Unnamed: 20']]

        elif year == '2017':
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 14', 'Unnamed: 15', 'Unnamed: 16', 'Unnamed: 17',
                                   'Unnamed: 18', 'Unnamed: 19', 'Unnamed: 20']]

        elif year == '2018':
            df_pool_list = df[1:][['Unnamed: 1', 'Unnamed: 13', 'Unnamed: 14', 'Unnamed: 15', 'Unnamed: 16',
                                   'Unnamed: 17', 'Unnamed: 18', 'Unnamed: 19']]

        df_pool_list['DATA_DATE'] = file_date

        if not df_col_extract.empty:
            df_col_extract = df_col_extract.append(df_pool_list, ignore_index=True)
        else:
            df_col_extract = df_pool_list
        num += 1
        print(df_pool_list.head())

    return df_col_extract


def main():
    # Define variables.
    process_year = "2018"
    process_yr = 18
    process_months = 1
    df_main = col_extract(process_year, process_months, process_yr)

    # Writing output to file.
    df_to_wind_notExcel(df_main, process_year)


main()
